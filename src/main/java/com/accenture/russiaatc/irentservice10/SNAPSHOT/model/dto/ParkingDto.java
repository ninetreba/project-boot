package com.accenture.russiaatc.irentservice10.SNAPSHOT.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ParkingDto {
    private Long id;
    private String name;
}
