package com.accenture.russiaatc.irentservice10.SNAPSHOT.configuration;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.math.BigDecimal;

@Getter
@ConfigurationProperties(prefix = "price")
public class PriceProperties {
    private  BigDecimal startingPriceBicycle;
    private  BigDecimal startingPriceElectricScooter;
    private  BigDecimal pricePerMinBicycle;
    private  BigDecimal pricePerMinElectricScooter;


}
